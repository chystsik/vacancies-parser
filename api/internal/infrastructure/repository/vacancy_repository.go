package repository

import (
	"fmt"
	"vacancy-parser/api/internal/models"
	"vacancy-parser/api/internal/service"
)

type vacancyRepository struct {
	data map[int]*models.Vacancy
}

const (
	errVacancyNotFound = "vacancy with id %d not found"
)

func NewVacancyRepository() service.VacancyRepository {
	return &vacancyRepository{data: make(map[int]*models.Vacancy)}
}

func (vr *vacancyRepository) Create(dto models.Vacancy) error {
	vr.data[dto.Id] = &dto
	return nil
}

func (vr *vacancyRepository) GetByID(id int) (models.Vacancy, error) {
	if vacancy, ok := vr.data[id]; ok {
		return *vacancy, nil
	}

	return models.Vacancy{}, fmt.Errorf(errVacancyNotFound, id)
}

func (vr *vacancyRepository) GetList() ([]models.Vacancy, error) {
	vacancies := make([]models.Vacancy, 0, len(vr.data))

	for _, vacancy := range vr.data {
		vacancies = append(vacancies, *vacancy)
	}

	return vacancies, nil
}

func (vr *vacancyRepository) Delete(id int) error {
	if _, ok := vr.data[id]; ok {
		delete(vr.data, id)
		return nil
	}

	return fmt.Errorf(errVacancyNotFound, id)
}
