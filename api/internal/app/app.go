package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"vacancy-parser/api/config"
	"vacancy-parser/api/internal/adapters"
	"vacancy-parser/api/internal/infrastructure/parser"
	"vacancy-parser/api/internal/infrastructure/repository"
	"vacancy-parser/api/internal/service"
	"vacancy-parser/api/internal/transport/restapi"

	"github.com/sirupsen/logrus"
)

const (
	errHttpListen   = "Failed to listen and serve (http): %v"
	errHttpShutdown = "HTTP server shutdown error: %v"

	logHttpStart       = "HTTP Server: started on port %v"
	logHttpStop        = "HTTP Server: stopped serving new connections..."
	logHttpShutdown    = "Graceful shutdown of HTTP Server complete."
	logSignalInterrupt = "Interrupt signal. Shutdown"
)

func Run(cfg *config.AppConfig) {
	// Initialising logger
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: time.RFC3339,
	})
	level := logrus.Level(cfg.Service.Loglevel)
	logger.SetLevel(level)
	// Initialising repository, service and handlers
	vacancyRepository := repository.NewVacancyRepository()
	vacancyParser := parser.NewVacancyParser(cfg.Parser, logger)
	vacancyService := service.NewVacancyService(vacancyRepository, vacancyParser)
	handlers := adapters.NewHandlers(vacancyService, cfg.Service.SwaggerUiFilePath, logger)

	// Startup http server with handlers
	server := restapi.NewServer(*cfg, handlers, logger)
	go func() {
		logger.Infof(logHttpStart, cfg.Http.Port)
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			logger.Fatalf(errHttpListen, err)
		}
		logger.Infoln(logHttpStop)
	}()

	// Graceful shutdown setup
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	logger.Infoln(logSignalInterrupt)
	ctxShutdown, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	// Graceful shutdown HTTP Server
	if err := server.Shutdown(ctxShutdown); err != nil {
		logger.Fatalf(errHttpShutdown, err)
	}
	logger.Infoln(logHttpShutdown)
}
