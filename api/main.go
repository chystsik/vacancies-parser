package main

import (
	"log"

	"vacancy-parser/api/config"
	"vacancy-parser/api/internal/app"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	app.Run(cfg)
}
