package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type (
	AppConfig struct {
		Http
		Service
		Parser
	}

	Http struct {
		Host string
		Port int
	}

	Service struct {
		PublicFolder      string
		SwaggerUiFilePath string
		Selectors
		Loglevel uint32
	}

	Parser struct {
		SeleniumHost     string
		VacancySiteUrl   string
		VacancySearchUrl string
		VacanciesPerPage uint
		Selectors
	}

	Selectors struct {
		VacancyCount string
		OnPageLinks  string
	}
)

const (
	errDecode = "unable to decode app config into struct, %v"
)

func New() (*AppConfig, error) {
	var cfg *AppConfig
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./api/config")
	viper.AddConfigPath("/usr/local/etc/vacancies-parser/config")
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, fmt.Errorf(errDecode, err)
	}
	return cfg, nil
}
